"""
FishAndCHIPs - Opcode interpreter
Opcode interpreter for CHIP8 emulator.
By Kat Hamer
"""

def dispatch_opcode(opcode):
    """Dispatch an opcode to it's function"""
    
class functions(object):
    """Functions that correspond to opcodes"""

    def __init__(self, chip8):
        self.chip8 = chip8
        self.display = chip8.display

        self.map = {"0000": self.call,
                    "00e0": self.clear_screen}

    def call(self, **args):
        raise NotImplemented

    def clear_screen(self):
        """Clear screen"""
        


