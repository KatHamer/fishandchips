"""
FishAndCHIPs - GUI layer
GUI for CHIP8 emulator. Handles screen pixel data and control of input
By Kat Hamer
"""

import tkinter
from tkinter import filedialog
import logging
from PIL import Image, ImageTk
import random
import chip8
import sys
import rom
import timeit

class Display(object):
    """Display"""

    def __init__(self, bitmap=None):
        self.scale = True
        self.scale_amount = 4

        self.pixel_off_color = (176, 255, 130)
        self.pixel_on_color = (40, 40, 40)

        self.display_size = (64, 32)
        self.columns, self.rows = self.display_size

        self.display_image = Image.new("RGB", self.display_size, self.pixel_off_color)
        self.display_image.save("screen.jpg")
        self.display_bitmap = self.display_image.load()

    def set_pixel(self, column, row, pixel_state):
        """Set a pixel"""
        if type(pixel_state) != bool:
            raise ValueError(f"Pixels can only be set to off or on! Not {pixel_state}")
        elif not column <= self.columns and row <= self.rows:
            raise ValueError(f"Pixel coordinates out of range! Screen is {self.columns}x{self.rows}")
        else:
            if not pixel_state:
                #logging.debug(f"Making pixel ({column},{row}) black.")
                color = self.pixel_on_color
            else:
                #logging.debug(f"Making pixel ({column},{row}) white.")
                color = self.pixel_off_color
            self.display_bitmap[column, row] = color
        self.display_image.save("screen.jpg")

    def scale_display(self, scale_factor=2):
        """Scale image up"""
        scale_width = self.columns * scale_factor
        scale_height = self.rows * scale_factor
        return self.display_image.resize((scale_width, scale_height))

class Dialog(object):
    def __init__(self, msg):

        self.root = None
        self.top = tkinter.Toplevel(self.root)
        self.master.resizable(False, False)

        self.frame = tkinter.Frame(self.top, borderwidth=4, relief='ridge')
        self.frame.pack(fill='both', expand=True)

        self.label = tkinter.Label(self.frame, text=msg)
        self.label.pack(padx=4, pady=4)

        b_ok = tkinter.Button(self.frame, text='Ok')
        b_ok['command'] = self.top.destroy
        b_ok.pack(padx=4, pady=4)

class App(tkinter.Frame):
    """Main class"""

    def __init__(self, root):
        super().__init__()
        logging.basicConfig(level=logging.DEBUG)
        self.running = True
        self.display = Display()
        self.chip = chip8.CHIP8(self.display)
        self.initUI()

    def resize_window(self):
        if self.display.scale:
            actual_screen_width = self.display.columns * self.display.scale_amount
            actual_screen_height = self.display.rows * self.display.scale_amount
        else:
            actual_screen_width = self.display.columns
            actual_screen_width = self.display.rows

        window_width = actual_screen_width
        window_height = actual_screen_height + 30

        self.master.geometry(f"{window_width}x{window_height}")


    def initUI(self):
        """Initialise UI"""
        self.master.title("FishAndCHIPS")
        self.resize_window()
        self.master.resizable(False, False)

        self.menubar = tkinter.Menu(self.master)
        self.master.config(menu=self.menubar)

        self.file_menu = tkinter.Menu(self.menubar)
        self.file_menu.add_command(label="Exit", command=self.do_exit)
        self.file_menu.add_command(label="Load ROM", command=self.load_chip_rom)

        self.scaling_menu = tkinter.Menu(self.menubar)
        self.scaling_menu.add_command(label="Scaling Off", command=lambda: self.adjust_scaling(1))
        self.scaling_menu.add_command(label="Scaling 2X", command=lambda: self.adjust_scaling(2))
        self.scaling_menu.add_command(label="Scaling 4X", command=lambda: self.adjust_scaling(4))
        self.scaling_menu.add_command(label="Scaling 8X", command=lambda: self.adjust_scaling(8))
        
        self.execution_menu = tkinter.Menu(self.menubar)
        self.execution_menu.add_command(label="Halt execution", command=lambda: self.set_running(0))
        self.execution_menu.add_command(label="Resume execution", command=lambda: self.set_running(1))
        
        self.menubar.add_cascade(label="File", menu=self.file_menu)
        self.menubar.add_cascade(label="Scaling", menu=self.scaling_menu)
        self.menubar.add_cascade(label="Execution", menu=self.execution_menu)
        
        self.frame = tkinter.Frame(self.master)
        self.frame.pack(fill="both", expand=True)

        self.image_label = tkinter.Label(self.frame)
        self.image_label.pack(padx=4, pady=4)

        self.status = tkinter.Label(self.master, text="Running...", bd=1, relief="sunken", anchor="w")
        self.status.pack(side="bottom", fill="x")

        logging.info("UI initialisation complete...")

    def do_exit(self):
        """Exit application"""
        #TODO: Add quit dialog
        logging.info("Exiting application...")
        sys.exit()

    def load_chip_rom(self):
        """Load a CHIP8 ROM"""
        filename = filedialog.askopenfilename(
                initialdir = "/",
                title = "Select CHIP-8 ROM",
                filetypes = (("CHIP-8 ROM files","*.rom"),)
        )

        rom_opcodes = rom.load_rom(filename)
        print("Loaded ROM...")
        for opcode in rom_opcodes:
            print(opcode)

    def adjust_scaling(self, scale_amount):
        self.display.scale = True
        self.display.scale_amount = scale_amount
        self.draw_display()
        self.resize_window()

    def set_running(self, value):
        if value:
            self.status.config(text="Running...")
        else:
            self.status.config(text="Halted...")
        self.running = value

    def draw_display(self):
        """Draw/update bitmap display"""
        if self.display.scale:
            self.image = self.display.scale_display(self.display.scale_amount)
        else:
            self.image = self.display.display_image

        self.image = ImageTk.PhotoImage(self.image)
        self.image_label.config(image=self.image)
        self.master.update()

    def loop(self):
        """Main loop"""
        self.draw_display()
        if self.running:
            self.chip.do_cycle
        self.update_status()
        self.master.update_idletasks()
        self.master.after(1, self.loop)
        #self.master.after(1, self.chip.update_timers())


def main():
    root = tkinter.Tk()
    app = App(root)
    root.after(0, app.loop)
    root.mainloop()


if __name__ == "__main__":
    main()

