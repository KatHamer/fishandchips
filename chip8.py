"""
FishAndCHIPs
A simple CHIP8 emulator, designed in order to understand
how an emulator is written
By Kat Hamer
"""

import time 
import random


"""
Memory map:
    0x000-0x1FF - Chip 8 interpreter (contains font set in emu)
    0x050-0x0A0 - Used for the built in 4x5 pixel font set (0-F)
    0x200-0xFFF - Program ROM and work RAM
"""


class CHIP8(object):
    """Main class"""

    def __init__(self, display):
        """Initialise system"""

        #TODO: Maybe restructure sections?

        """Initialise outputs (screen, buzzer)"""
        self.display = display

        """Initialise memory, CPU registers and screen"""
        self.memory = []  # Store memory
        self.v_registers = []  # Store CPU registers
        self.screen = []  # Store pixel states
        
        """Initialise timers"""
        self.delay_timer = 0  # Standard timer
        self.sound_timer = 0  # System buzzes when timer reaches 0

        """Initialise stack, counter, current opcode, index register and stack pointer"""
        self.stack = []  # Represent stack as a list using .pop and .push
        self.program_counter = 0x200  # Program counter starts at 0x200
        self.opcode = 0  # Reset current opcode
        self.index_register = 0  # Reset index register
        self.stack_pointer = 0  # Reset stack pointer

        """Initialise keypad"""
        self.keypad = []  # Initialise HEX keypad
        
    def update_timers(self):
        """Update timers"""
        if self.delay_timer > 0:
            self.delay_timer -= 1  # Decrement display timer if it is above 0
        
        if self.sound_timer > 0:
            self.delay_timer -= 1  # Decrement sound timer if it is above 0

        if self.sound_timer == 0:
            #outputs.sound_buzzer(1)  # Sound buzzer for 1 second if the sound timer reaches 0
            print("Beep!")

    def do_cycle(self):
        """Emulate a single CPU cycle"""
        self.display.set_pixel(
                random.randint(0, 63),
                random.randint(0, 31),
                random.choice([True, False])
        )
