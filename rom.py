"""
FishAndCHIPs - Rom loader
Read CHIP-8 ROMs
By Kat Hamer
"""

def load_rom(filename):
    """Load a ROM"""
    with open(filename, "rb") as fp:
        rom_data = fp.read()

    singles = [hex(line) for line in rom_data]
    padded_singles = [single[2:].zfill(2) for single in singles]
    pairs = zip(padded_singles[::2], padded_singles[1::2]) 

    opcodes = [a+b for a, b in pairs]
    return opcodes
 
